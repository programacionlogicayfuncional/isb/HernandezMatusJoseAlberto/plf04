(ns plf04.core)

;1
(defn stringE-1
  [x]
  (letfn [(f [x y]
            (if (empty? x)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first x))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f x 0)))

(stringE-1 "Hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele")
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")

(defn stringE-2
  [x y]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (if (and
                   (> acc 0)
                   (< acc 4))
                true
                false)
              (if (and (= \e (first xs)))
                (f (rest xs) (+ acc 1))
                (f (rest xs) acc))))]
    (f x y)))

(stringE-2 "Hello" 0)
(stringE-2 "Heelle" 0)
(stringE-2 "Heelele" 0)
(stringE-2 "Hll" 0)
(stringE-2 "e" 0)
(stringE-2 "" 0)

;2
(defn string-times-1
  [x y]
  (if (== y 0)
    ""
    (str x (string-times-1 x (dec y)))))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [x y]
  (letfn [(f [x y acc]
            (if (or (zero? y) (empty? x))
              acc
              (str x (f x (dec y) (str acc)))))]
    (f x y "")))

(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

;3
(defn front-times-1
  [lengt c]
  (letfn [(f [x y]
            (if (< (count x) 3)
              (if (zero? y)
                ""
                (str lengt (f x (dec y))))
              (if (zero? y)
                ""
                (str (subs lengt 0 3) (f x (dec y))))))]
    (f lengt c)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [lengt c acc]
  (letfn [(f [x y z]
            (if (< (count x) 3)
              (if (zero? y)
                z
                (f x (dec y) (str x z)))
              (if (zero? y)
                z
                (f x (dec y) (str (subs x 0 3) z)))))]
    (f lengt c acc)))

(front-times-2 "Chocolate" 2 "")
(front-times-2 "Chocolate" 3 "")
(front-times-2 "Abc" 3 "")
(front-times-2 "Ab" 4 "")
(front-times-2 "A" 4 "")
(front-times-2 "" 4 "")
(front-times-2 "Abc" 0 "")

;4
(defn countxx-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              z
              (if (and (= \x (first y)) (= \x (first (rest y))))
                (f (rest y) (inc z))
                (f (rest y) z))))]
    (f x 0)))

(countxx-1 "abcxx")
(countxx-1 "xxx")
(countxx-1 "xxxx")
(countxx-1 "abc")
(countxx-1 "Hello there")
(countxx-1 "Hexxo thxxe")
(countxx-1 "")
(countxx-1 "Kittens")
(countxx-1 "Kittensxxx")

(defn countxx-2
  [x]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= z (first y)) (= z (first (rest y))))
                (f (rest y) z (inc acc))
                (f (rest y) z acc))))]
    (f x \x 0)))

(countxx-2 "abcxx")
(countxx-2 "xxx")
(countxx-2 "xxxx")
(countxx-2 "abc")
(countxx-2 "Hello there")
(countxx-2 "Hexxo thxxe")
(countxx-2 "")
(countxx-2 "Kittens")
(countxx-2 "Kittensxxx")

;5
(defn string-splosion-1
  [x]
  (letfn [(f [y]
            (if (== 0 (count y))
              y
              (apply str (f (subs y 0 (- (count y) 1))) y)))]
    (f x)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "there")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [x save]
  (letfn [(f [y acc]
            (if (== 0 (count y))
              acc
              (f (subs y 0 (- (count y) 1)) (str y acc))))]
    (f x save)))

(string-splosion-2 "Code" "")
(string-splosion-2 "abc" "")
(string-splosion-2 "ab" "")
(string-splosion-2 "x" "")
(string-splosion-2 "fade" "")
(string-splosion-2 "there" "")
(string-splosion-2 "Kitten" "")
(string-splosion-2 "Bye" "")
(string-splosion-2 "Good" "")
(string-splosion-2 "Bad" "")

;6
(defn array123-1
  [xs]
  (letfn [(f [ys]
            (if (and (= 1 (first ys))
                     (= 2 (first (rest ys)))
                     (= 3 (first (rest (rest ys)))))
              true (if (empty? ys) false (f (rest ys)))))]
    (f xs)))

(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,4,1])
(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,1,2,3])
(array123-1 [1,1,2,1,2,1])
(array123-1 [1,2,3,1,2,3])
(array123-1 [1,2,3])
(array123-1 [1,1,1])
(array123-1 [1,2])
(array123-1 [1])
(array123-1 [])

(defn array123-2
  [bx]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (and acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (and acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f bx "")))

(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,4,1])
(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,1,2,3])
(array123-2 [1,1,2,1,2,1])
(array123-2 [1,2,3,1,2,3])
(array123-2 [1,2,3])
(array123-2 [1,1,1])
(array123-2 [1,2])
(array123-2 [1])
(array123-2 [])

;7
(defn stringX-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (== 0 z)
                (str (first y) (f (rest y) (inc z)))
                (if (and (= \x (first y)) (> (count y) 1))
                  (f (rest y) (inc z))
                  (str (first y) (f (rest y) (inc z)))))))]
    (f x 0)))

(stringX-1 "xxHxix")
(stringX-1 "abxxxcd")
(stringX-1 "xabxxxcdx")
(stringX-1 "xKittenx")
(stringX-1 "Hello")
(stringX-1 "xx")
(stringX-1 "x")
(stringX-1 "")

(defn stringx-2
  [x]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (== 0 y)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (= \x (first x)) (> (count x) 1))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f x 0 "")))

(stringx-2 "xxHxix")
(stringx-2 "abxxxcd")
(stringx-2 "xabxxxcdx")
(stringx-2 "xKittenx")
(stringx-2 "Hello")
(stringx-2 "xx")
(stringx-2 "x")
(stringx-2 "")

;8
(defn altPairs-1
  [x]
  (letfn [(f [y z]
            (if (== (count y) z)
              ""
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (str (subs y z (+ z 1)) (f x (inc z)))
                (f x (inc z)))))]
    (f x 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [x]
  (letfn [(f [y z acc]
            (if (== (count y) z)
              acc
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (f y (inc z) (str acc (subs y z (+ z 1))))
                (f y (inc z) acc))))]
    (f x 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOTher")

;9
(defn stringYak-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z))
                (str (first y) (f (rest y) (inc z))))))]
    (f x 0)))

(stringYak-1 "yakpak")
(stringYak-1 "pakyak")
(stringYak-1 "yak123ya")
(stringYak-1 "yak")
(stringYak-1 "yakxxxyak")
(stringYak-1 "HiyakHi")
(stringYak-1 "xxxyakyyyakzzz")

(defn stringYak-2
  [x]
  (letfn [(f [y z acc]
            (if (empty? y)
              acc
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z) acc)
                (f (rest y) (inc z) (str acc (first y))))))]
    (f x 0 "")))

(stringYak-2 "yakpak")
(stringYak-2 "pakyak")
(stringYak-2 "yak123ya")
(stringYak-2 "yak")
(stringYak-2 "yakxxxyak")
(stringYak-2 "HiyakHi")
(stringYak-2 "xxxyakyyyakzzz")

;10
(defn has271-1
  [x]
  (letfn [(f [ys]
            (if (or (<= (count ys) 2) (empty? ys))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys)))))]
    (f x)))

(has271-1 [1 2 7 1])
(has271-1 [1 2 8 1])
(has271-1 [2 7 1])
(has271-1 [3 8 2])
(has271-1 [2 7 3])
(has271-1 [2 7 4])
(has271-1 [2 7 -1])
(has271-1 [2 7 -2])
(has271-1 [4 5 3 8 0])
(has271-1 [2 7 5 10 4])
(has271-1 [2 7 -2 4 9 3])
(has271-1 [2 7 5 10 1])
(has271-1 [2 7 -2 4 10 2])
(has271-1 [1 1 4 9 0])
(has271-1 [1 1 4 9 4 9 2])

(defn has271-2
  [x acc]
  (letfn [(f [ys y]
            (if (empty? ys)
              false
              (if (>= (count ys) 3)
                (if (and (== (- (first (rest ys)) (first ys)) 5)
                         (>= 2 (if (neg? (- (first (rest (rest ys))) (- (first ys) 1)))
                                 (* -1 (- (first (rest (rest ys))) (- (first ys) 1)))
                                 (* 1 (- (first (rest (rest ys))) (- (first ys) 1))))))
                  true
                  (f (rest ys) y))
                (f (empty ys) y))))]
    (f x acc)))

(has271-2 [1 2 7 1] "")
(has271-2 [1 2 8 1] "")
(has271-2 [2 7 1] "")
(has271-2 [3 8 2] "")
(has271-2 [2 7 3] "")
(has271-2 [2 7 4] "")
(has271-2 [2 7 -1] "")
(has271-2 [2 7 -2] "")
(has271-2 [4 5 3 8 0] "")
(has271-2 [2 7 5 10 4] "")
(has271-2 [2 7 -2 4 9 3] "")
(has271-2 [2 7 5 10 1] "")
(has271-2 [2 7 -2 4 10 2] "")
(has271-2 [1 1 4 9 0] "")
(has271-2 [1 1 4 9 4 9 2] "")

